package hotelroyal.resources.fonts;


import java.awt.Font;
import java.io.InputStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author JStalin
 */
public class FuenteCityScape {

    private Font fuente = null;

    public FuenteCityScape() {
        String nombreFuente = "cityscape font.ttf";
        try {
            //Se carga la fuente
            InputStream is = getClass().getResourceAsStream(nombreFuente);
            fuente = Font.createFont(Font.TRUETYPE_FONT, is);
        } catch (Exception ex) {
            //Si existe un error se carga fuente por defecto ARIAL
            System.err.println(nombreFuente + " No se cargo la fuente");
            fuente = new Font("Arial", Font.PLAIN, 14);
        }
    }

    /* Font.PLAIN = 0 , Font.BOLD = 1 , Font.ITALIC = 2
 * tamanio = float
     */
    public Font getFuente(int estilo, float tamanio) {
        Font tfont = fuente.deriveFont(estilo, tamanio);

        return tfont;
    }

}
